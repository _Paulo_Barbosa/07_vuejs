import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import TaskIndex from '@/components/TaskIndex'
import TaskCreate from '@/components/TaskCreate'
import TaskUpdate from '@/components/TaskUpdate'
import TaskShow from '@/components/TaskShow'
import TaskFinished from '@/components/TaskFinished'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Home
    },
    {
      path: '/tasks',
      name: 'TaskIndex',
      component: TaskIndex
    },
    {
      path: '/tasks/create',
      name: 'TaskCreate',
      component: TaskCreate
    },    
    {
      path: '/tasks/:id/update',
      name: 'TaskUpdate',
      component: TaskUpdate
    },
    {
      path: '/tasks/:id',
      name: 'TaskShow',
      component: TaskShow
    },
    {
      path: '/tasks',
      name: 'TaskFinished',
      component: TaskFinished
    }


  ]
})
